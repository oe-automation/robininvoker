﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using RobinInvoker.ConsoleHelpers;

namespace RobinInvoker.ConsoleHelpers
{
    /// <summary>
    /// Static console methods for printing info
    /// </summary>
    public static class ConsoleFormatters
    {
        /// <summary>
        /// Class which holds console formatting setup methods and calls
        /// </summary>
        public static class ConsoleFontFormat
        {
            // Console Font Infos
            private const int FixedWidthTrueType = 54;
            private const int StandardOutputHandle = -11;
            private static readonly IntPtr ConsoleOutputHandle = Win32CallStructs.GetStdHandle(StandardOutputHandle);

            /// <summary>
            /// Changes and stores the console font.
            /// </summary>
            /// <param name="FontName">Name of font to set</param>
            /// <param name="FontSize">Size of the font</param>
            /// <param name="ShowNewInfo">Print info or not.</param>
            /// <returns></returns>
            public static Win32CallStructs.FontInfo[] SetCurrentFont(string FontName, short FontSize = 0, bool ShowNewInfo = false)
            {
                // Print out some info if set
                if (ShowNewInfo) Console.WriteLine("Set Current Font: " + FontName);

                // Check if the current font is new or not.
                Win32CallStructs.FontInfo CurrentFont = new Win32CallStructs.FontInfo { cbSize = Marshal.SizeOf<Win32CallStructs.FontInfo>() };
                if (Win32CallStructs.GetCurrentConsoleFontEx(ConsoleOutputHandle, false, ref CurrentFont))
                {
                    // Build new font info setup for the new font
                    Win32CallStructs.FontInfo NewFontToSet = new Win32CallStructs.FontInfo
                    {
                        cbSize = Marshal.SizeOf<Win32CallStructs.FontInfo>(),
                        FontIndex = 0,
                        FontFamily = FixedWidthTrueType,
                        FontName = FontName,
                        FontWeight = 400,
                        FontSize = FontSize > 0 ? FontSize : CurrentFont.FontSize
                    };

                    // Get some settings from current font.
                    if (!Win32CallStructs.SetCurrentConsoleFontEx(ConsoleOutputHandle, false, ref NewFontToSet))
                    {
                        var ex = Marshal.GetLastWin32Error();
                        Console.WriteLine("Set error " + ex);
                        throw new System.ComponentModel.Win32Exception(ex);
                    }

                    // Check new font setup
                    Win32CallStructs.FontInfo NewlySetFont = new Win32CallStructs.FontInfo { cbSize = Marshal.SizeOf<Win32CallStructs.FontInfo>() };
                    Win32CallStructs.GetCurrentConsoleFontEx(ConsoleOutputHandle, false, ref NewlySetFont);

                    // Return new font info
                    return new[] { CurrentFont, NewFontToSet, NewlySetFont };
                }
                else
                {
                    // Get last error and print if wanted.
                    var WinError = Marshal.GetLastWin32Error();
                    if (!ShowNewInfo) { throw new System.ComponentModel.Win32Exception(WinError); }

                    // Format and print error.
                    var OldFg = Console.ForegroundColor; var OldBg = Console.BackgroundColor;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine("Get error " + WinError);
                    Console.ForegroundColor = OldFg;
                    Console.BackgroundColor = OldBg;

                    // Throw the ex or return.
                    throw new System.ComponentModel.Win32Exception(WinError);
                }
            }
        }


        // Print Helper Methods
        /// <summary>
        /// Prints message to console in the middle of the screen.
        /// </summary>
        /// <param name="Message">Message to print.</param>
        /// <param name="FgColor">Foreground color</param>
        /// <param name="BgColor">Background color</param>
        public static void PrintCenter(string Message, Win32CallStructs.ConsoleMessageConfig ConsoleConfig = default)
        {
            // Check config for empty.
            if (ConsoleConfig.ToString().Contains("NO_CONFIGURATION"))
            {
                // Setup new config values
                ConsoleConfig = new Win32CallStructs.ConsoleMessageConfig()
                {
                    SplitterChar = '=',
                    ShowDivider = false,
                    SeperatorRowCount = 0,
                    TitleTextColorPadding = 5,
                    MessageForegound = ConsoleColor.White,
                    DividerForegound = ConsoleColor.DarkGray,
                    DividerBackground = ConsoleColor.Black,
                    MessageBackground = ConsoleColor.Black,
                };
            }

            // Fix Odd Length Strings
            // if (Message.Length % 2 != 0) { Message = " " + Message; }

            // Build Div String
            string DivString = string.Empty;
            for (int DivLength = 0; DivLength < Console.WindowWidth; DivLength += 1) DivString += ConsoleConfig.SplitterChar;

            // If Divider in use, print it.
            if (ConsoleConfig.ShowDivider)
            {
                // Write divider string
                Console.ForegroundColor = ConsoleConfig.DividerForegound;
                Console.BackgroundColor = ConsoleConfig.DividerBackground;
                Console.Write(DivString);

                // Write padding rows.
                for (int RowCount = 0; RowCount < ConsoleConfig.SeperatorRowCount; RowCount += 1)
                {
                    // Write pipe and move to end
                    Console.Write('|');
                    int TopColorStart = ((Console.WindowWidth - Message.Length) / 2) - ConsoleConfig.TitleTextColorPadding - 1;
                    int TopColorStop = 2 + (TopColorStart + Message.Length) + ConsoleConfig.TitleTextColorPadding * 2;
                    if (Message.Length % 2 == 0) { TopColorStop -= 2; }
                    else { TopColorStart += 1; TopColorStop += 1; }
                    for (int PadRowLength = 0; PadRowLength < Console.WindowWidth - 2; PadRowLength += 1)
                    {
                        // Check if within the padding value
                        if ((PadRowLength >= TopColorStart && PadRowLength < TopColorStop) && RowCount <= 1)
                        {
                            // Set Message colors
                            Console.ForegroundColor = ConsoleConfig.MessageForegound;
                            Console.BackgroundColor = ConsoleConfig.MessageBackground;
                        }
                        else
                        {
                            // Set Div Colors
                            Console.ForegroundColor = ConsoleConfig.DividerForegound;
                            Console.BackgroundColor = ConsoleConfig.DividerBackground;
                        }

                        // Write space value
                        Console.Write(" ");
                    }

                    // Pipe Closing
                    Console.Write("|");
                }

                // Write pipe for title
                Console.Write('|');
                Console.ForegroundColor = ConsoleConfig.MessageForegound;
                Console.BackgroundColor = ConsoleConfig.MessageBackground;
            }

            // If Console Config is default, make a new one. Set and write.
            int LeftMsgColorStart = ((Console.WindowWidth - Message.Length) / 2) - ConsoleConfig.TitleTextColorPadding - 1;
            int LeftMsgColorStop = 2 + LeftMsgColorStart + (Message.Length + ConsoleConfig.TitleTextColorPadding);
            if (Message.Length % 2 == 0) { LeftMsgColorStop -= 2; }
            else { LeftMsgColorStart += 1; LeftMsgColorStop -= 1; }
            for (int WidthLeft = Console.CursorLeft; WidthLeft < ((Console.WindowWidth - Message.Length) / 2) + (Message.Length % 2 != 0 ? 1 : 0); WidthLeft += 1)
            {
                // Check if within the padding value
                if (WidthLeft > LeftMsgColorStart && WidthLeft <= LeftMsgColorStop)
                {
                    // Set Message colors
                    Console.ForegroundColor = ConsoleConfig.MessageForegound;
                    Console.BackgroundColor = ConsoleConfig.MessageBackground;
                }
                else
                {
                    // Set Div Colors
                    Console.ForegroundColor = ConsoleConfig.DividerForegound;
                    Console.BackgroundColor = ConsoleConfig.DividerBackground;
                }

                // Write space value
                Console.Write(" ");
            }

            // Write message value here
            if (Message.Length % 2 != 0) { Console.Write(" "); }
            Console.ForegroundColor = ConsoleConfig.MessageForegound;
            Console.Write(Message);

            // Write right end padding
            int RightMsgColorStart = Console.CursorLeft;
            int RightMsgColorStop = Console.CursorLeft + ConsoleConfig.TitleTextColorPadding + (Message.Length % 2 != 0 ? 1 : 0);
            for (int WidthRight = Console.CursorLeft; WidthRight < Console.WindowWidth - 1; WidthRight += 1)
            {
                // Check if within the padding value
                if (WidthRight >= RightMsgColorStart && WidthRight < RightMsgColorStop)
                {
                    // Set Message colors
                    Console.ForegroundColor = ConsoleConfig.MessageForegound;
                    Console.BackgroundColor = ConsoleConfig.MessageBackground;
                }
                else
                {
                    // Set Div Colors
                    Console.ForegroundColor = ConsoleConfig.DividerForegound;
                    Console.BackgroundColor = ConsoleConfig.DividerBackground;
                }

                // Write space value
                Console.Write(" ");
            }

            // If not div, seperate and return.
            if (!ConsoleConfig.ShowDivider)
            {
                // Newline
                Console.Write("\n\n");

                // Reset console Values here.
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                return;
            }

            // Set Colors
            Console.ForegroundColor = ConsoleConfig.DividerForegound;
            Console.BackgroundColor = ConsoleConfig.DividerBackground;
            Console.Write('|');

            // Write padding rows.
            // Write padding rows.
            for (int RowCount = 0; RowCount < ConsoleConfig.SeperatorRowCount; RowCount += 1)
            {
                Console.Write('|');
                int BottomColorStart = ((Console.WindowWidth - Message.Length) / 2) - ConsoleConfig.TitleTextColorPadding - 1;
                int BottomColorStop = 2 + (BottomColorStart + Message.Length) + ConsoleConfig.TitleTextColorPadding * 2;
                if (Message.Length % 2 == 0) { BottomColorStop -= 2; }
                else { BottomColorStart += 1; BottomColorStop += 1; }
                for (int PadRowLength = 0; PadRowLength < Console.WindowWidth - 2; PadRowLength += 1)
                {
                    // Check if within the padding value
                    if ((PadRowLength >= BottomColorStart && PadRowLength < BottomColorStop) && RowCount <= 1)
                    {
                        // Set Message colors
                        Console.ForegroundColor = ConsoleConfig.MessageForegound;
                        Console.BackgroundColor = ConsoleConfig.MessageBackground;
                    }
                    else
                    {
                        // Set Div Colors
                        Console.ForegroundColor = ConsoleConfig.DividerForegound;
                        Console.BackgroundColor = ConsoleConfig.DividerBackground;
                    }

                    // Write space value
                    Console.Write(" ");
                }

                // Pipe Closing
                Console.Write("|");
            }

            // Set Colors, close pipe
            Console.ForegroundColor = ConsoleConfig.DividerForegound;
            Console.BackgroundColor = ConsoleConfig.DividerBackground;
            Console.Write(DivString);

            // Reset console Values here.
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        /// <summary>
        /// Split the console following the desired format
        /// </summary>
        /// <param name="ConsoleConfig">Format to follow</param>
        public static void DivideConsole(Win32CallStructs.ConsoleMessageConfig ConsoleConfig = default)
        {
            // Check config for empty.
            if (ConsoleConfig.ToString().Contains("NO_CONFIGURATION"))
                ConsoleConfig = StaticConsoleStyles.DivideConsoleFormat;

            // Write splitter
            Console.ForegroundColor = ConsoleConfig.MessageForegound;
            Console.BackgroundColor = ConsoleConfig.MessageBackground;
            for (int WidthIndex = 0; WidthIndex < Console.WindowWidth; WidthIndex += 1)
                Console.Write(ConsoleConfig.SplitterChar);

            // Reset color
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
