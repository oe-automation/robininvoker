﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RobinInvoker.ConsoleHelpers
{
    /// <summary>
    /// Win32 DLL Calls from internal code
    /// </summary>
    public static class Win32CallStructs
    {
        #region DLL Import Calls
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern IntPtr GetStdHandle(int nStdHandle);
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern bool SetCurrentConsoleFontEx(IntPtr hConsoleOutput, bool MaximumWindow, ref FontInfo ConsoleCurrentFontEx);
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern bool GetCurrentConsoleFontEx(IntPtr hConsoleOutput, bool MaximumWindow, ref FontInfo ConsoleCurrentFontEx);
        #endregion

        #region Structure Definitions

        // Font info struct
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct FontInfo
        {
            internal int cbSize;
            internal int FontIndex;
            internal short FontWidth;
            public short FontSize;
            public int FontFamily;
            public int FontWeight;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            //[MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.wc, SizeConst = 32)]
            public string FontName;
        };

        // Info for console colors being set.
        public struct ConsoleMessageConfig
        {
            // Char info
            public char SplitterChar;                   // Char to put on the console to split lines
            public bool ShowDivider;                    // Shows the divider or not.

            // Title Height info
            public int SeperatorRowCount;               // Number of rows up and down to pad title text with
            public int TitleTextColorPadding;           // Padding value of color text background for title

            // Colors
            public ConsoleColor MessageForegound;       // Text color
            public ConsoleColor MessageBackground;      // Text background  
            public ConsoleColor DividerForegound;       // Divider Fg
            public ConsoleColor DividerBackground;      // Divider Bg

            // --------------------------------------------------------------------------------------------------------------------------------------------------------

            /// <summary>
            /// ToString override for comparison
            /// </summary>
            /// <returns>String version of this object</returns>
            public override string ToString()
            {
                // First check for all black color values
                if (this.MessageForegound == ConsoleColor.Black && this.MessageBackground == ConsoleColor.Black &&
                    this.DividerForegound == ConsoleColor.Black && this.DividerBackground == ConsoleColor.Black)
                    return "NO_CONFIGURATION";

                // Build string object here.
                string ReturnString = $"[SplitChar: {this.SplitterChar}][ShowDiv: {this.ShowDivider.ToString().ToUpper()}]";
                ReturnString += $"[MsgFg: {this.MessageForegound.ToString().ToUpper()}][MsgBg: {this.MessageBackground.ToString().ToUpper()}]";
                ReturnString += $"[DivFg: {this.DividerForegound.ToString().ToUpper()}][DivBg: {this.DividerBackground.ToString().ToUpper()}]";

                // Return string here
                return ReturnString;
            }
        }
        #endregion
    }
}
