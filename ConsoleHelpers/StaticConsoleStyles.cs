﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobinInvoker.ConsoleHelpers;

namespace RobinInvoker.ConsoleHelpers
{
    /// <summary>
    /// Static class for all console configs
    /// </summary>
    public static class StaticConsoleStyles
    {
        // Title Print info
        public static readonly Win32CallStructs.ConsoleMessageConfig TitleMessageFormat = new Win32CallStructs.ConsoleMessageConfig
        {
            SplitterChar = '=',
            ShowDivider = true,
            SeperatorRowCount = 1,
            TitleTextColorPadding = 5,
            MessageForegound = ConsoleColor.White,
            MessageBackground = ConsoleColor.Blue,
            DividerForegound = ConsoleColor.Yellow,
            DividerBackground = ConsoleColor.DarkBlue,
        };

        // Console Divider Print info
        public static readonly Win32CallStructs.ConsoleMessageConfig DivideConsoleFormat = new Win32CallStructs.ConsoleMessageConfig
        {
            SplitterChar = '=',
            ShowDivider = true,
            SeperatorRowCount = 1,
            TitleTextColorPadding = 5,
            MessageForegound = ConsoleColor.DarkGray,
            MessageBackground = ConsoleColor.Black,
            DividerForegound = ConsoleColor.Black,
            DividerBackground = ConsoleColor.Black,
        };

        // Config for console printer output during tasks
        public static readonly Win32CallStructs.ConsoleMessageConfig StartingTaskInfo = new Win32CallStructs.ConsoleMessageConfig
        {
            SplitterChar = '=',
            ShowDivider = false,
            SeperatorRowCount = 1,
            TitleTextColorPadding = 5,
            MessageForegound = ConsoleColor.DarkBlue,
            MessageBackground = ConsoleColor.Yellow,
            DividerForegound = ConsoleColor.Black,
            DividerBackground = ConsoleColor.Black,
        };
        public static readonly Win32CallStructs.ConsoleMessageConfig TaskCompletedInfo = new Win32CallStructs.ConsoleMessageConfig
        {
            SplitterChar = '=',
            ShowDivider = false,
            SeperatorRowCount = 1,
            TitleTextColorPadding = 5,
            MessageForegound = ConsoleColor.DarkGreen,
            MessageBackground = ConsoleColor.Yellow,
            DividerForegound = ConsoleColor.Black,
            DividerBackground = ConsoleColor.Black,
        };
    }
}
