﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

// Logging and printing
using static RobinInvoker.ConsoleHelpers.ConsoleFormatters;
using static RobinInvoker.ConsoleHelpers.StaticConsoleStyles;

// Robin Engine and objects
using RobinEngine = Microsoft.Flow.RPA.Desktop.Robin.Engine;
using Microsoft.Flow.RPA.Desktop.Robin.Core.Contexts.Statements.Enums;
using RobinProgram = Microsoft.Flow.RPA.Desktop.Robin.Core.Contexts.Scopes;
using Microsoft.Flow.RPA.Desktop.Robin.Core.StatementList;
using Microsoft.Flow.RPA.Desktop.Robin.SDK;
using Microsoft.Flow.RPA.Desktop.Robin.SDK.Images;
using Microsoft.Flow.RPA.Desktop.Robin.SDK.Controls;
using Microsoft.Flow.RPA.Desktop.Robin.Runtime.Agent;
using Microsoft.Flow.RPA.Desktop.Robin.Core.Symbols;
using Microsoft.Flow.RPA.Desktop.Robin.Engine.Packager.Interfaces;
using Microsoft.Flow.RPA.Desktop.Robin.Engine.Validation.SymbolTypeScope;

namespace RobinInvoker
{
    /// <summary>
    /// Main File invoker for a robin process
    /// </summary>
    public class RobinInvokerMakin
    {
        // Path values for robin.
        public static string RobinDirectoryPath;

        // Files to track
        public static string RobinScirpt;
        public static string RobinAppMask;
        public static string RobinImgRepo;

        // Robin Objects
        public static RobinEngine.Engine EngineObject;     // Robin engine
        public static RobinProgram.Program ProgramObject;  // Program to run

        // Image and Control Repo
        public static ImageRepository ImageRepo;            // Images 
        public static ControlRepository ControlRepo;        // Controls

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Invokes a robin script object
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            // Title Print
            Console.SetWindowSize(110, 50);
            PrintCenter("ROBIN INVOKER V0.1 - THIS MAY OR MAY NOT DO ANYTHING", TitleMessageFormat);
            if (args.Length == 0) { throw new ArgumentException("NOT ENOUGH ARGUMENTS PROVIDED!"); }
            Console.WriteLine();

            // Get the path from args[0]
            RobinDirectoryPath = args[0];
            if (RobinDirectoryPath == null) { throw new FileNotFoundException("ERROR! INPUT PATH TO ROBIN DIR WAS NULL!"); }
            if (!Directory.Exists(RobinDirectoryPath)) { throw new DirectoryNotFoundException("INPUT DIR FOR ROBIN NOT FOUNT!"); }

            // Get the files from the directory.
            PrintCenter("Looking for Robin Requirements...", StartingTaskInfo);
            if (!GetRobinFiles(RobinDirectoryPath)) { throw new FileNotFoundException("ERROR! FAILED TO FIND ONE OR MORE ROBIN FILES!"); }
            Console.WriteLine($"--> ROBIN PATH FROM ARGS:  {RobinDirectoryPath}");
            Console.WriteLine($"--> \\__ Script Path:       {RobinScirpt}");
            Console.WriteLine($"--> \\__ App Mask Path:     {RobinAppMask}");
            Console.WriteLine($"--> \\__ Imagfe Reop Path:  {RobinScirpt}\n");
            PrintCenter("Found Robin Files OK!", TaskCompletedInfo);

            // Split Console
            DivideConsole();

            // Get Robin Statement objects now.
            Console.WriteLine();
            PrintCenter("Looking for Robin Statements...", StartingTaskInfo);
            Console.WriteLine("--> READING CONTENTS OF ROBIN FILE NOW AND UPDATING IMPORT STATEMENTS");
            Console.WriteLine("--> SEARCHER NOW BUILDING CONTROL AND IMAGE REPOS");
            if (!BuildRepos()) { throw new InvalidOperationException("FAILED TO GENERATE IMAGE OR CONTROL REPO OF PROGRAM FROM ROBIN FILE!"); }
            Console.WriteLine("--> IMPORTED REPOS FOR IMAGES AND CONTROLS OK!");
            string FileContent = ReplaceImportStatements(File.ReadAllText(RobinScirpt));
            Console.WriteLine("--> FINDING ROBIN STATEMENT VALUES NOW");
            if (!BuildRobinStatements(FileContent)) { throw new InvalidOperationException("FAILED TO GENERATE CONTENTS OF PROGRAM FROM ROBIN FILE!"); }
            Console.WriteLine("--> SEARCHER COMPLETED LOOKING FOR ROBIN STATEMENTS");
            Console.WriteLine($"--> LOCATED A TOTAL OF {ProgramObject.Statements.Count} STATEMENTS PULLED IN FROM FILE!");
            Console.WriteLine($"--> AND A TOTAL OF {ProgramObject.Functions.Count} FUNCTIONS!\n");
            PrintCenter("Built Robin Program Object and Engine OK!", TaskCompletedInfo);

            // Split Console
            DivideConsole();
            Console.WriteLine();

            // Execute the robin object here.
            PrintCenter("Executing Robin Program Object Now...", StartingTaskInfo);
            Console.WriteLine("--> TRYING TO EXECUTE PROGRAM OBJECT. THIS MAY TAKE A BIT");
            if (!RunRobinProgram())
            {
                Console.WriteLine();
                PrintCenter("FAILED TO EXECUTE ROBIN ENGINE PROGRAM OBJECT!", TaskCompletedInfo);
            }
            else
            {
                Console.WriteLine("--> ROBIN PROGRAM OBJECT HAS BEEN CONSUMED, AND RUN CORRECTLY!");
                Console.WriteLine("--> THIS MEANS AN AUTOMATION FLOW SHOULD BE GOING/COMPLETED BY NOW\n");
                PrintCenter("Robin Program Object Executed OK!", TaskCompletedInfo);
            }

            // Readline to keep console open
            DivideConsole();
            Console.ReadLine();
        }


        /// <summary>
        /// Gets the paths of the robin files.
        /// </summary>
        private static bool GetRobinFiles(string RobinPath)
        {
            // Find the script, app mask and the imgrepo
            var FilesFound = Directory.GetFiles(RobinPath);
            RobinScirpt = Path.Combine(RobinDirectoryPath, new FileInfo(FilesFound.First(FileObj => FileObj.EndsWith("robin"))).Name);
            RobinAppMask = Path.Combine(RobinDirectoryPath, new FileInfo(FilesFound.First(FileObj => FileObj.EndsWith("appmask"))).Name);
            RobinImgRepo = Path.Combine(RobinDirectoryPath, new FileInfo(FilesFound.First(FileObj => FileObj.EndsWith("imgrepo"))).Name);

            // Check if passed.
            return RobinScirpt != null && RobinAppMask != null && RobinImgRepo != null;
        }
        /// <summary>
        /// Replaces old import file content values with current ones if needed.
        /// </summary>
        /// <param name="RobinFileContent">Content to modify</param>
        /// <returns>Updated content</returns>
        private static string ReplaceImportStatements(string RobinFileContent)
        {
            // Start with AppMask then do img repo
            string ReplacePath = RobinDirectoryPath + Path.DirectorySeparatorChar;

            // Store lines and update
            var SplitContent = RobinFileContent.Split('\r');
            SplitContent[0] = "IMPORT '" + ReplacePath + new FileInfo(RobinAppMask).Name + "' AS appmask\r\n";
            SplitContent[1] = "IMPORT '" + ReplacePath + new FileInfo(RobinImgRepo).Name + "' AS imgrepo\r\n";

            // Return new string.
            RobinFileContent = string.Join("", SplitContent);
            RobinFileContent += "\r\n";
            return RobinFileContent;
        }
        /// <summary>
        /// Builds an invoker to call the robin package
        /// </summary>
        private static bool BuildRobinStatements(string RobinFileContent)
        {
            // Build content, try to append value
            RobinEngine.Startup.RegisterDefaults();
            EngineObject = new RobinEngine.Engine();
            var BuiltStatements = EngineObject.Parser.GetStatementList(RobinFileContent);
            BuiltStatements.ImageRepositories = new List<ImageRepositorySymbol>();
            BuiltStatements.ControlRepositories = new List<ControlRepositorySymbol>();

            // Store repo values
            BuiltStatements.ImageRepositories.Add(new ImageRepositorySymbol("imgrepo", ImageRepo));
            BuiltStatements.ControlRepositories.Add(new ControlRepositorySymbol("appmask", ControlRepo));

            // Cast into a program now,
            EngineObject.Validate(BuiltStatements);
            ProgramObject = BuiltStatements.ToProgram();

            // Validate content and return
            return true;
        }
        /// <summary>
        /// BUilds the image and control repos
        /// </summary>
        /// <returns>True if built, false if not.</returns>
        private static bool BuildRepos()
        {
            try
            {
                // Build images and controls, then return out.
                ImageRepo = ImageRepository.DeserializeFromJsonFile(RobinImgRepo);
                ControlRepo = ControlRepository.DeserializeFromJsonFile(RobinAppMask);
                return true;
            }
            catch { return false; }
        }
        /// <summary>
        /// Try and run the newly made robin object
        /// </summary>
        /// <returns>True if run, false if not.</returns>
        private static bool RunRobinProgram()
        {
            // Pack project output 
            string OutputDir = Path.Combine(Directory.GetCurrentDirectory(), "ProgramPackages", ProgramObject.Id.ToString());
            RobinEngine.Packager.PackageHelper.PackProgramToDirectory(ProgramObject, OutputDir);

            // Run engine, store result from run request
            bool EngineRunOK = EngineObject.Runner.Run(ProgramObject, RobinEngine.RunMode.Run);
            if (!EngineRunOK)
            {
                Console.WriteLine();
                int IndexOfException = 1;
                var ExecutionErrors = EngineObject.Runner.ErrorHandler.Errors;
                foreach (var ExError in ExecutionErrors)
                {
                    try
                    {
                        // Write error info out
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"--> EXCEPTION {IndexOfException}:");
                        Console.BackgroundColor = ConsoleColor.Black;

                        // Write Ex Values
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write($"    \\__ Ex Msg: "); Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(ExError.Description.Replace("\r\n", " "));
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write($"    \\__ Ex ID:  "); Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(ExError.Id);

                        // Tick Counter
                        IndexOfException += 1;
                    }
                    catch { IndexOfException += 1; continue; }
                }
            }

            // Print results
            return EngineRunOK;
        }
    }
}
